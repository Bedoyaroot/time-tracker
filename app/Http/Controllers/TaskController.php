<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use DateTime;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(["items" => Task::all()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $task = Task::create($request->all());
        return response()->json($task, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {

        if ($task->start_time != null){
            $time1 = Carbon::createFromDate($task->start_time);
            $time2 = Carbon::now();
            $task->elapsed_time = $time1->diffInSeconds($time2);
        }

        return response()->json($task, 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $task->update($request->all());
        return response()->json($task, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return response()->json(["message" => "La tarea '$task->name' ha sido eliminada con exito!"], 200);
    }

    public function assign($id_task, $id_project) 
    {
        $task = Task::findOrFail($id_task);
        $project = Project::findOrFail($id_project);
        $task->project_id = $project->id;
        $task->update();

        return response()->json($task);
    }

    public function start(Task $task) 
    {
        if(!$task->project_id) {
            return response()->json(["message" => "La tarea '$task->name' no tiene un proyecto asignado"], 300);
        }
        $running = $task->project->tasks->where('run', true)->first();
        if($running) {
            return response(["message" => "La tarea '$running->name' esta iniciada actualmente"], 300);
        }
        else {
            $task = Task::find($task->id);
            $task->start_time = Carbon::now();
            $task->run = true;
            $task->update();
            return response()->json($task, 200);
        }
    }

    public function stop(Task $task) 
    {
        if(!$task->run) {
            return response()->json(["message" => "Esta tarea no se ha iniciado aun"], 200);
        }
        else {
            $time1 = Carbon::createFromDate($task->start_time);
            $time2 = Carbon::now();
            $task->elapsed_time += $time1->diffInSeconds($time2);
            $task->run = false;
            $task->update();
            return response()->json($task, 200);
        }
    }
}
