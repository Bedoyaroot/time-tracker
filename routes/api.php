<?php

use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('tasks', TaskController::class);
Route::put('/tasks/start/{task}', [TaskController::class, 'start'])->name('tasks.start');
Route::put('/tasks/stop/{task}', [TaskController::class, 'stop'])->name('tasks.stop');
Route::put('/tasks/assign/{id_task}/{id_project}', [TaskController::class, 'assign'])->name('tasks.assign');
Route::apiResource('projects', ProjectController::class);
Route::get('/project/report/{project}', [ProjectController::class, 'report'])->name('projects.report');
